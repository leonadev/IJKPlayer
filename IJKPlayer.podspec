Pod::Spec.new do |s|
  s.name         = "IJKPlayer"
  s.version      = "0.0.1"
  s.summary      = "IJKPlayer k0.8.3"
  s.description  = <<-DESC
                   IJKPlayer
                   DESC
  s.homepage     = "https://gitlab.com/leonadev/IJKPlayer"
  s.license      = 'MIT'
  s.author       = "leonadev"
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://gitlab.com/leonadev/IJKPlayer.git", :tag => s.version.to_s }
  s.source_files  = "*"
  s.frameworks = "VideoToolbox","UIKit","QuartzCore","OpenGLES","MobileCoreServices","MediaPlayer","CoreVideo","CoreMedia","CoreGraphics","AVFoundation","AudioToolbox"
  s.vendored_frameworks = "IJKMediaFramework.framework"
end
